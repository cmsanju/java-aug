package com.test.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.test.model.Employee;
import com.test.model.Employees;

@Repository
@Service
public class EmpService {
		
	static Employees empDt = new Employees();
	
	static
	{
		empDt.getEmpList().add(new Employee(1, "Ramanuj", "Blr"));
		empDt.getEmpList().add(new Employee(2, "Das", "Chn"));
		empDt.getEmpList().add(new Employee(3, "Rakesh", "Hyd"));
		empDt.getEmpList().add(new Employee(4, "Gupta", "Pune"));
	}
	
	//read employee data
	public Employees getAllEmployees()
	{
		return empDt;
	}
	
	//create employee data
	public void addEmployee(Employee emp)
	{
		empDt.getEmpList().add(emp);
	}
	
	//update employee data based on id
	public String updateEmployee(Employee empObj)
	{
		for(int i = 0; i < empDt.getEmpList().size(); i++)
		{
			Employee e = empDt.getEmpList().get(i);
			
			if(empObj.getId().equals(e.getId()))
			{
				empDt.getEmpList().set(i, empObj);
				
			}
		}
		
		return "the given id is not available";
	}
	//delete employee record based on id
	public String deleteEmployee(Integer id)
	{
		for(int i = 0; i < empDt.getEmpList().size(); i++)
		{
			Employee e = empDt.getEmpList().get(i);
			
			if(e.getId().equals(id))
			{
				empDt.getEmpList().remove(i);
			}
		}
		
		return "the given id is not available";
	}
}
