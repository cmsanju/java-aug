package com.test.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.test.model.Employee;
import com.test.model.Employees;

@Repository
@Service
public class EmpService {
	
	static Employees emp = new Employees();
	
	static
	{
		emp.getEmpList().add(new Employee(1, "Nikesh", "ABC"));
		emp.getEmpList().add(new Employee(2, "Shivam", "hcl"));
		emp.getEmpList().add(new Employee(3, "gupta", "pwc"));
		emp.getEmpList().add(new Employee(4, "Rakesh", "Dell"));
		emp.getEmpList().add(new Employee(5, "Ranjan", "dxc"));
	}
	
	//display all records
	public Employees getAllEmployees()
	{
		return emp;
	}
	
	//add employee record
	public void addEmployee(Employee emp1)
	{
		emp.getEmpList().add(emp1);
	}
	
	//update employee record
	public String updateEmployee(Employee emp1)
	{
		for(int i = 0; i < emp.getEmpList().size(); i++)
		{
			Employee e = emp.getEmpList().get(i);
			if(e.getId().equals(emp1.getId()))
			{
				emp.getEmpList().set(i, emp1);
			}
		}
		
		return "the given id is not available";
	}
	
	//delete employee record
	public String deleteEmployee(Integer id)
	{
		for(int i = 0; i < emp.getEmpList().size(); i++)
		{
			Employee emp1 = emp.getEmpList().get(i);
			
			if(emp1.getId().equals(id))
			{
				emp.getEmpList().remove(i);
			}
		}
		
		return "the given id is not available";
	}
}
