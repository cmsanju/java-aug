package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.model.Employee;
import com.test.model.Employees;
import com.test.service.EmpService;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmpService empServie;
	
	@GetMapping(value = "/listemp", produces = "application/json")
	public Employees getAll()
	{
		return empServie.getAllEmployees();
	}
	
	@PostMapping(value = "/addemp", consumes = "application/json")
	public Employees createEmployee(@RequestBody Employee emp)
	{
		int id = empServie.getAllEmployees().getEmpList().size() + 1;
		
		emp.setId(id);
		
		empServie.addEmployee(emp);
		
		return empServie.getAllEmployees();
	}
	
	@PutMapping(value = "/empupdate/{id}", consumes = "application/json")
	public Employees updateEmployee(@PathVariable("id")int id, @RequestBody Employee emp)
	{
		emp.setId(id);
		
		empServie.updateEmployee(emp);
		
		return empServie.getAllEmployees();
	}
	
	@DeleteMapping(value = "/empdelete/{id}", produces = "application/json")
	public Employees deleteEmploye(@PathVariable("id")int id)
	{
		empServie.deleteEmployee(id);
		
		return empServie.getAllEmployees();
	}

}
